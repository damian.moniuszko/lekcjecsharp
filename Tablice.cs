//Zadanie 1

int[] dane = new int[10];

//for (int i = 0; i < dane.Length; i++)
//{
//    dane[i] = i;
//}

//foreach (int i in dane)
//{
//    Console.WriteLine(i);
//}

//Zadanie 2

//int a = 9;

//for (int i = 0; i < dane.Length; i++)
//{
//    dane[a] = i;
//    a--;
//}

//foreach (int i in dane)
//{
//    Console.WriteLine(i);
//}

//Zadanie 3

int[,] macierz = new int[10, 10];

//for (int i = 0; i < macierz.GetLength(0); i++)
//{
//    for (int j = 0; j < macierz.GetLength(1); j++)
//    {
//        if (i == j)
//        {
//            macierz[i, j] = 1;
//        }
//        else
//        {
//            macierz[i, j] = 0;
//        }
//    }
//}

//foreach (int i in macierz)
//{
//    Console.WriteLine(i);
//}

//Zadanie 4

//int a = 0;

//for (int i = 0; i < macierz.GetLength(0); i++)
//{
//    for (int j = 0; j < macierz.GetLength(1); j++)
//    {
//        if (i == j)
//        {
//            macierz[i, j] = a;
//            a++;
//        }
//        else
//        {
//            macierz[i, j] = 0;
//        }
//    }
//}

//foreach (int i in macierz)
//{
//    Console.WriteLine(i);
//}

//Zadanie 5

//for (int i = 0; i < macierz.GetLength(0); i++)
//{
//    for (int j = 0; j < macierz.GetLength(1); j++)
//    {
//        if (i + j == 9)
//        {
//            macierz[i, j] = 1;
//        }
//        else
//        {
//            macierz[i, j] = 0;
//        }
//    }
//}

//foreach (int i in macierz)
//{
//    Console.WriteLine(i);
//}

//Zadanie 6

//int a = 0;

//for (int i = 0; i < macierz.GetLength(0); i++)
//{
//    for (int j = 0; j < macierz.GetLength(1); j++)
//    {
//        if (i + j == 9)
//        {
//            macierz[i, j] = a;
//            a++;
//        }
//        else
//        {
//            macierz[i, j] = 0;
//        }
//    }
//}

//foreach (int i in macierz)
//{
//    Console.WriteLine(i);
//}

//Zadanie 7



for (int i = 0; i < macierz.GetLength(0); i++)
{
    for (int j = 0; j < macierz.GetLength(1); j++)
    {
        if (j == 0)
        {
            macierz[i, j] = i;
        }
        else if(j == 1){
            macierz[i, j] = (int)Math.Pow(i, 2);
        }
    }
}

foreach (int i in macierz)
{
    Console.WriteLine(i);
}
